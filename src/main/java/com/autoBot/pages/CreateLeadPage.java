package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class CreateLeadPage extends Annotations {
	
 	public CreateLeadPage enterCompanyName(String data) {
		//driver.findElementById("createLeadForm_companyName").sendKeys(data);
 		WebElement eleCompanyName = locateElement("id", "createLeadForm_companyName");
 		clearAndType(eleCompanyName,data);
		return this;
	}
	
	public CreateLeadPage enterFirstName(String data) {
		//driver.findElementById("createLeadForm_firstName").sendKeys(data);
 		WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
 		clearAndType(eleFirstName,data);
		return this;
	}
	public CreateLeadPage enterLastName(String data) {
		//driver.findElementById("createLeadForm_lastName").sendKeys(data);
 		WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
 		clearAndType(eleLastName,data);
		return this;
	}
 
	public ViewLeadPage clickCreateLeadButton() {
		//driver.findElementByClassName("smallSubmit").click();
		WebElement eleCreateButton = locateElement("class", "smallSubmit");
		click(eleCreateButton);  
		return new ViewLeadPage();
	}
	
	

}
