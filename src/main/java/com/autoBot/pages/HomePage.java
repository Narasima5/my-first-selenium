package com.autoBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.autoBot.testng.api.base.Annotations;


public class HomePage extends Annotations{ 

	/*public HomePage() {
       PageFactory.initElements(driver, this);
	} 
*/
//	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogout;
	public LoginPage clickLogout() {
		WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleLogout);  
		return new LoginPage();
	}

	public HomePage verifyLoginName(String data) {
		String loginName = driver.findElementByTagName("h2").getText();
		if(loginName.contains(data)) {
			System.out.println("Login success");
		}else {
			System.out.println("Logged username mismatch");
		}
		return this;
	}
	
	public MyHomePage clickCRMSFALink() {
		//driver.findElementByLinkText("CRM/SFA").click();
		WebElement eleCRMSFA = locateElement("link", "CRM/SFA");
		click(eleCRMSFA);  
		return new MyHomePage();
	}
	

}







