package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class MyHomePage extends Annotations {
	
	public MyLeadsPage clickLeadsLink()
	{
		//driver.findElementByLinkText("Leads").click();
		WebElement eleLeads = locateElement("link", "Leads");
		click(eleLeads);  
		return new MyLeadsPage();
	}
	

}
