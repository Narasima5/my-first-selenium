package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;


public class MyLeadsPage extends Annotations {
	
	public CreateLeadPage clickCreateLeadLink()
	{
	    //driver.findElementByLinkText("Create Lead").click();
		WebElement eleCreateLead = locateElement("link", "Create Lead");
		click(eleCreateLead);  
	    return new CreateLeadPage();

	}
	

}
