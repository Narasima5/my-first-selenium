package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;

public class TC002_CreateLead_Positive extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_CreateLead_Positive";
		testcaseDec = "Login into leaftaps";
		author = "satish";
		category = "smoke";
		excelFileName = "TC001";
	} 

	@Test(dataProvider="getData") 
	public void CreateLead_Positive(String uName, String pwd,String logInName,String companyName, String firstName, String lastName) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.verifyLoginName(logInName)
		.clickCRMSFALink()
		.clickLeadsLink()
		.clickCreateLeadLink()
		.enterCompanyName(companyName)
		.enterFirstName(firstName)
		.enterLastName(lastName)
		.clickCreateLeadButton()
		.verifyFirstName();
		//.clickLogout();
		
	}
	
}






